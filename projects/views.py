from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateProjectForm


@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": projects,
    }
    return render(request, "projects/projects_list.html", context)


@login_required
def individual_project(request, id):
    project = get_object_or_404(Project, id=id)
    context = {
        "individual_project": project,
    }
    return render(request, "projects/individual_project.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateProjectForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            form.save()
            return redirect("list_projects")
    else:
        form = CreateProjectForm
    context = {
        "form": form,
    }
    return render(request, "projects/create_project.html", context)
