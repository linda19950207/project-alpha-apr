from django.urls import path
from . import views

urlpatterns = [
    path("create/", views.create_tasks, name="create_task"),
    path("mine/", views.assignee_tasks, name="show_my_tasks"),
]
